# this import statement is needed if you want to use the AWS Lambda Layer called "pytorch-v1-py36"
# it unzips all of the pytorch & dependency packages when the script is loaded to avoid the 250 MB unpacked limit in AWS Lambda
try:
    import unzip_requirements
except ImportError:
    pass

import json
import os
import os.path
import sys
import boto3
from botocore.exceptions import ClientError
import logging
from io import BytesIO
import re
import numpy as np
from PIL import Image
import torch
from torchvision import transforms

from tg_client import TgClient
from s3_helper import S3Helper

log = logging.getLogger()
log.setLevel(logging.DEBUG)

from environment import BUCKET, PRETRAINED_BUCKET, SQS_RNN_NAME

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

#
# Initializing Decoder
#

from beheaded_inception3 import beheaded_inception_v3

s3 = boto3.client('s3')

inception_pretrained_key = "inception_v3_google-1a9a5a14.pth"
inception_pretrained_file_path = \
    S3Helper.download_if_not_exist(
        s3_client = s3,
        bucket_name = PRETRAINED_BUCKET,
        key = inception_pretrained_key)

decoder = beheaded_inception_v3(pretrained_file_path = inception_pretrained_file_path).train(False)

imsize = 299

content_transform = \
    transforms.Compose([
        transforms.Resize((imsize, imsize)),
        transforms.ToTensor(),
    ])

def handler(event, context):
    
    s3 = boto3.client('s3')

    for record in event['Records']:

        message = json.loads(record["body"])
        img_key = str(message['img_key'])
        chat_id = str(message['chat_id'])        

        tg_client = TgClient(chat_id)

        try:
        
            tg_client.send_message("Итак, посмотрим :)")

            img_file_path = f"/tmp/{img_key}"

            s3.download_file(
                BUCKET, 
                img_key, 
                img_file_path)

            image = Image.open(img_file_path).convert('RGB')
            image = content_transform(image)

            _, vectors_neck, _ = decoder(image[None])

            vectors_neck_key = f"{img_key}.pkl"
            vectors_neck_file_path = f"/tmp/{vectors_neck_key}"

            torch.save(vectors_neck, vectors_neck_file_path)

            with open(vectors_neck_file_path, 'rb') as vectors_neck_data:
                s3 \
                    .put_object(
                        Bucket = BUCKET,
                        ACL = 'public-read',
                        Key = vectors_neck_key,
                        Body = vectors_neck_data)

            sqs = boto3.client('sqs')

            queue_url = sqs.get_queue_url(QueueName = SQS_RNN_NAME)['QueueUrl']

            sqs.send_message(
                QueueUrl = queue_url,
                MessageBody = json.dumps({ 'vectors_neck_key': vectors_neck_key, 'chat_id': chat_id }))

        except Exception as e:
            
            tg_client.send_message('CNN, Упс, ошибочка - {}'.format(e))