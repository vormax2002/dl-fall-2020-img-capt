import random
import torch, torch.nn as nn
import torch.nn.functional as F

class CaptionNet(nn.Module):

    def __init__(
        self, 
        vocab,
        cnn_feature_size, 
        embed_size, 
        hidden_size,
        max_captions_len,
        device):

        super(self.__class__, self).__init__()

        # стандартная архитектура такой сети такая: 
        # 1. линейные слои для преобразования эмбеддиинга картинки в начальные состояния h0 и c0 LSTM-ки
        # 2. слой эмбедднга
        # 3. несколько LSTM слоев (для начала не берите больше двух, чтобы долго не ждать)
        # 4. линейный слой для получения логитов
    
        self.vocab = vocab
        vocab_size = len(vocab)
        self.max_captions_len = max_captions_len
        self.device = device

        # линейный слой, чтобы перевести фичи картинки на входе в 
        # эмбеддинг для RNN (2048 из Inception V3 слишком много для эмбеддинга слова)
        self.features_to_embed =\
            nn.Linear(
                in_features = cnn_feature_size,
                out_features = embed_size)

        self.hidden_size = hidden_size
        
        # эмбеддинг для слов
        self.word_embeddings = nn.Embedding(vocab_size, embed_size)
        
        # инициализация LSTM. 1 слой - пробовал с 2, что не привело к заметному улучшению
        # качества при такой архитектуре, плюс ограничения AWS Lambda по RAM
        self.lstm =\
            nn.LSTM(
                input_size = embed_size, 
                hidden_size = hidden_size,
                num_layers = 1,
                bias = True,
                batch_first = True,
                dropout = 0,
                bidirectional = False,
            )
        
        # линейный слой из скрытого состояния в one-hot в словаре
        self.linear = nn.Linear(hidden_size, vocab_size)

    # первичная инициализация скрытого слоя нулями
    def init_hidden(self, batch_size):
        
        return (torch.zeros((1, batch_size, self.hidden_size), device = self.device), \
                torch.zeros((1, batch_size, self.hidden_size), device = self.device))
        
    # генерация описания
    # variations_treshold - размер отклонения от максимального выхода, который мы тоже
    # считаем правильным ответом (0.1 - значит могут отличаться на 10% максимум), таким
    # образом рандомно получаем различные варианты декодирования, и различные варианты описаний
    def generate_caption(self, inputs, variations_treshold):
        
        output = []
        batch_size = inputs.shape[0] # (1, 1, embed_size)
        hidden = self.init_hidden(batch_size)
    
        inputs = self.features_to_embed(inputs)

        for _ in range(self.max_captions_len):

            lstm_out, hidden = self.lstm(inputs, hidden) # (1, 1, hidden_size)
            outputs = self.linear(lstm_out)  # (1, 1, vocab_size)
            outputs = outputs.squeeze(1) # (1, vocab_size)
            
            # если разница между том выходами меньше variations_treshold - выбираем рандомно
            # возьмем топ 5, так как больше не имеет смысла
            max_values, max_indices = torch.topk(outputs, 5)

            max_values = max_values.squeeze(0)
            max_indices = max_indices.squeeze(0)

            max_indice = max_indices[0]
            list_ids_for_random_choice = [0]

            # пробежимся по топу ответом, и посмотрим сколько из них можно считать
            # правильными
            for i in range(1, len(max_values)):
                if abs(max_values[0] - max_values[i]) < variations_treshold * abs(max_values[0]):
                    list_ids_for_random_choice.append(i)
                    
            # рандомно выберем из списка правильных ответов                    
            max_indice = max_indices[random.choice(list_ids_for_random_choice)]

            max_indice = max_indice.unsqueeze(0)
            
            # если предсказали конец - выходим
            if max_indice == self.vocab.get_eos():
                break

            # игнорим специальные слова
            if max_indice != self.vocab.get_sos() and \
               max_indice != self.vocab.get_unk() and \
               max_indice != self.vocab.get_pad():
                output.append(max_indice.cpu().numpy()[0].item())
            
            # готовим инпут для следующей итерации - берем эмбеддинг предсказанного слова
            inputs = self.word_embeddings(max_indice) # (1, embed_size)
            inputs = inputs.unsqueeze(1) # (1, 1, embed_size)
            
        return output