import os
import sys
import boto3
from botocore.exceptions import ClientError

class S3Helper():
    
    @staticmethod
    def is_file_exist(s3_client, bucket_name, file_key):
        try:            
            s3_client.get_object(
                Bucket=bucket_name,
                Key=file_key)

            return True
        except ClientError as ex:
            if(ex.response['Error']['Code'] == 'NoSuchKey'):
                return False
            else:
                raise

    @staticmethod
    def get_file_path(file_key):
        return f"/tmp/{file_key}"

    @staticmethod
    def download_if_not_exist(s3_client, bucket_name, key):
        file_path = S3Helper.get_file_path(key)

        if not os.path.isfile(file_path):
            s3_client.download_file(
                bucket_name,
                key, 
                file_path)

        return file_path