import json
import os
import os.path

# класс словаря, сильно упрощен по сравнению с ноутбуком
# (удален код генерации)
class Vocabulary(object):

    def __init__(
        self,
        word2idx_file,
        idx2word_file):

        self.word2idx_file = word2idx_file
        self.idx2word_file = idx2word_file
        self.get_vocab()

    def get_pad(self):
        return self.word2idx["<pad>"]

    def get_sos(self):
        return self.word2idx["<sos>"]

    def get_eos(self):
        return self.word2idx["<eos>"]

    def get_unk(self):
        return self.word2idx["<unk>"]

    def get_vocab(self):
        self.word2idx = json.load(open(self.word2idx_file))
        self.idx2word = json.load(open(self.idx2word_file))

    def __call__(self, word):
        if not word in self.word2idx:
            return self.get_unk()
        return self.word2idx[word]

    def __len__(self):
        return len(self.word2idx)