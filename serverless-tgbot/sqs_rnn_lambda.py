# this import statement is needed if you want to use the AWS Lambda Layer called "pytorch-v1-py36"
# it unzips all of the pytorch & dependency packages when the script is loaded to avoid the 250 MB unpacked limit in AWS Lambda
try:
    import unzip_requirements
except ImportError:
    pass

import json
import os
import os.path
import sys
import boto3
from botocore.exceptions import ClientError
import logging
from io import BytesIO
import re
import numpy as np
from PIL import Image
import torch
from torchvision import transforms

from tg_client import TgClient
from s3_helper import S3Helper

log = logging.getLogger()
log.setLevel(logging.DEBUG)

from environment import BUCKET, PRETRAINED_BUCKET

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

s3 = boto3.client('s3')

#
# Initializing Vocabulary
#

from vocabulary import Vocabulary

vocab_word2idx_key = "vocab_word2idx.json"
vocab_word2idx_file_path = \
    S3Helper.download_if_not_exist(
        s3_client = s3,
        bucket_name = PRETRAINED_BUCKET,
        key = vocab_word2idx_key)

vocab_idx2word_key = "vocab_idx2word.json"
vocab_idx2word_file_path = \
    S3Helper.download_if_not_exist(
        s3_client = s3,
        bucket_name = PRETRAINED_BUCKET,
        key = vocab_idx2word_key)

vocab = \
    Vocabulary(
        word2idx_file = vocab_word2idx_file_path,
        idx2word_file = vocab_idx2word_file_path)

#
# Initializing Decoder
#

from caption_net import CaptionNet

decoder = \
    CaptionNet(
        vocab = vocab,
        cnn_feature_size = 2048, 
        embed_size = 300, 
        hidden_size = 512,
        max_captions_len = 57,
        device = device)

decoder_key = "decoder.pth"
decoder_file_path = \
    S3Helper.download_if_not_exist(
        s3_client = s3,
        bucket_name = PRETRAINED_BUCKET,
        key = decoder_key)

decoder.load_state_dict(torch.load(decoder_file_path, map_location = device))
decoder.eval()

def handler(event, context):
    
    s3 = boto3.client('s3')

    for record in event['Records']:

        message = json.loads(record["body"])
        vectors_neck_key = str(message['vectors_neck_key'])
        chat_id = str(message['chat_id'])        

        tg_client = TgClient(chat_id)

        try:
        
            tg_client.send_message(f"Вот мои варианты:")

            vectors_neck_file_path = \
                S3Helper.download_if_not_exist(
                    s3_client = s3,
                    bucket_name = BUCKET,
                    key = vectors_neck_key)

            vectors_neck = torch.load(vectors_neck_file_path)

            # запоминаем те варианты, которые уже отправили,
            # чтобы исключить дубли
            already_sent = []

            for _ in range(10):
                sentence = \
                    generate_caption(
                        decoder = decoder,
                        vocab = vocab,
                        vectors_neck = vectors_neck)

                if not sentence in already_sent:
                    tg_client.send_message(sentence)
                    already_sent.append(sentence)

        except Exception as e:
            
            tg_client.send_message('RNN, Упс, ошибочка - {}'.format(e))
        
# переводим массив токенов предсказания декодера в строку со словами
def detokenize_sentence(output, vocab = vocab):

    list_string = []
    
    for idx in output:
        list_string.append(vocab.idx2word[f"{idx}"])
    
    # убираем ненужный пробел перед точкой в конце предложения,
    # и заодно решаем проблему дублей когда точки в конце предложения
    # нет. Для начала удалим точку, если она есть.
    if len(list_string) > 0 and list_string[len(list_string)-1] == ".":
        list_string.pop()

    # джойним слова через пробел
    sentence = ' '.join(list_string)

    # добавляем точку в конец
    sentence = f"{sentence}."

    # делаем предложение с большой буквы
    sentence = sentence.capitalize()
    
    return sentence

# генерируем описание
def generate_caption(decoder, vocab, vectors_neck):

    result = ""

    with torch.no_grad():
        caption = decoder.generate_caption(vectors_neck.to(device).unsqueeze(0), variations_treshold = 0.1)
        result = detokenize_sentence(output = caption, vocab = vocab)
            
    return result
