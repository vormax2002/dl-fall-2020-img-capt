import json
import os
import sys
import boto3
import logging

from tg_client import TgClient
from s3_helper import S3Helper

log = logging.getLogger()
log.setLevel(logging.DEBUG)

from environment import BUCKET, SQS_CNN_NAME

def handler(event, context):
    
    log.debug("logging test")    

    try:
        data = json.loads(event["body"])
        chat_id = data['message']['chat']['id']        

        tg_client = TgClient(chat_id)
        s3 = boto3.client('s3')

        try:

            message_id = data['message']['message_id']
            first_name = data["message"]["chat"]["first_name"]            

            # received photo
            if 'photo' in data['message']:

                # get file with the best resolution
                resolutions_num = len(data['message']['photo'])
                file_id = str(data['message']['photo'][resolutions_num-1]['file_id'])

                file_content = tg_client.get_file(file_id)

                file_name = '{}_{}.jpg'.format(chat_id, message_id)

                s3 \
                    .put_object(
                        Bucket = BUCKET,
                        ACL = 'public-read',
                        Key = file_name,
                        Body = file_content)   

                sqs = boto3.client('sqs')

                queue_url = sqs.get_queue_url(QueueName = SQS_CNN_NAME)['QueueUrl']

                sqs.send_message(
                    QueueUrl = queue_url,
                    MessageBody = json.dumps({ 'img_key': file_name, 'chat_id': chat_id }))

                tg_client.send_message('Спокойствие, поставил в очередь на обработку...')
           
            # received text only - send instructions
            else:

                instructions = \
                    f'Привет, {first_name}!\n' + \
                    'Пришли фотографию и я расскажу, что на ней происходит!'

                tg_client.send_message(instructions)                                

        except Exception as e:

            tg_client.send_message('Упс, ошибочка - {}'.format(e))

    except Exception as e:

        print(e)

    return { "statusCode": 200 }